# Directus AJAX

This project was created to make it easy to work with a headless cms api created with directus.  It includes authentication and your standard REST methods.  Take it or leave it.  I built it for my use but hopefully others can find it useful.

It uses qwest to help with the AJAX requests and uses PubSub to publish and subscribe to the requests.  

## Authentication

Coming soon..

## User Profile

Coming soon..

## Methods

Support for all the standard REST methods.  Details coming soon..
### Get
Coming soon..
### Post
Coming soon..
### Put
Coming soon..
### Patch
Coming soon..
### Delete
Coming soon..

## PubSub
All AJAX requests will have a PubSub method tied to them.

### Authentication
#### auth:login
Login event.

#### auth:logout
Logout event.

#### auth:loading
In the process or authenticating or logging out events.

## Notifications
Coming soon..
