import nodeResolve from 'rollup-plugin-node-resolve';
import uglify from 'rollup-plugin-uglify';
import { minify } from 'uglify-es';

export default {
  input: 'src/index.js',
  output: {
    file: 'dist/directus.ajax.js',
    format: 'es'
  },
  plugins: [
    nodeResolve({
      jsnext: true,
      main: true
    }),
    uglify({}, minify)
  ],
  globals: {
    qwest: 'qwest'
  }
};
