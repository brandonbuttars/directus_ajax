import 'qwest/qwest.min';
import * as Config from './config';
import AuthAjax from './scripts/auth.ajax';
import PubSubClass from './scripts/pubsub.ajax';
import Notifications from './scripts/notifications';

// App Object
window.App = {};

// Configuration
App.url = Config.url;

// PubSub
App.pubsub = new PubSubClass;

// Notifications
App.notifications = new Notifications;

// Qwest AJAX Configuration
qwest.base = App.url.base + App.url.api;
qwest.setDefaultOptions(Config.qwest);

// Initialize Authentication
App.auth = new AuthAjax('local');

// PubSub App Subscriptions
App.pubsub.subscribe('authenticated', data => {
  // console.log('token', data.token);
});

App.pubsub.subscribe('logged-out', () => {
  // console.log('logged out..');
});
