// https://davidwalsh.name/pubsub-javascript
// PubSub Class
class PubSubClass {
  constructor() {
    this.topics = {};
    this.hop = this.topics.hasOwnProperty;
  }

  subscribe(topic, listener) {
    let self = this;
    // Create the topic's object if not yet created
    if (!self.hop.call(self.topics, topic)) { self.topics[topic] = []; }

    // Add the listener to queue
    let index = self.topics[topic].push(listener) - 1;

    // Provide handle back for removal of topic
    return {
      remove: function () {
        delete self.topics[topic][index];
      }
    };
  }

  publish(topic, info) {
    let self = this;
    // If the topic doesn't exist, or there's no listeners in queue, just leave
    if (!self.hop.call(self.topics, topic)) { return; }

    // Cycle through topics queue, fire!
    self.topics[topic].forEach(function (item) {
      item(info !== undefined ? info : {});
    });
  }
}

export default PubSubClass;
