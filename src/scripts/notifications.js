class AppNotifications {
  constructor() {
    window.addEventListener('error', obj => {
      // TODO: Capture more error details
      this.publish('error', obj);
    });

    App.pubsub.subscribe('notification:success', obj => {
      this.alert_box(obj.message);
    });
    
    App.pubsub.subscribe('notification:info', obj => {
      this.alert_box(obj.message);
    });
    
    App.pubsub.subscribe('notification:warning', obj => {
      this.alert_box(obj.message);
    });

    App.pubsub.subscribe('notification:error', obj => {
      this.alert_box(obj.message);
    });
  }

  publish(type, obj) {
    App.pubsub.publish(`notification:${type}`, obj);
  }
  
  alert_box(obj) {
    // Basic alert
    alert(obj);
  }
}

export default AppNotifications;
// TODO: Make better and different notification options with more details.
