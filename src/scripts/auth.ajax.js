class AuthAjax {
  constructor(type = 'session') {
    this.store = type === 'local' ? localStorage : sessionStorage;

    App.pubsub.subscribe('auth:logout', () => {
      this.logout;
      setTimeout(() => {
        document.body.removeAttribute('authenticated');
        document.body.removeAttribute('authenticating');
        // App.pubsub.publish('notification:success', {
        //   message: 'Successfully logged out!'
        // });
      }, 500);
    });

    App.pubsub.subscribe('auth:loading', () => {
      document.body.setAttribute('authenticating', '');
    });

    App.pubsub.subscribe('auth:login', () => {
      this.get_profile(this.email);
      setTimeout(() => {
        document.body.setAttribute('authenticated', '');
        document.body.removeAttribute('authenticating');
        // App.pubsub.publish('notification:success', {
        //   message: 'Successfully logged in!'
        // });
      }, 500);
    });

    App.pubsub.subscribe('auth:error', error => {
      setTimeout(() => {
        document.body.removeAttribute('authenticating');
        // throw new Error(error);
        App.pubsub.publish('notification:error', {
          message: error.message
        });
      }, 500);
    });

    document.querySelector('form[auth-login]').addEventListener('submit', e => {
      e.preventDefault();
      
      function get_val(key) {
        return e.target.querySelector(`[name=${key}]`).value;
      }
      
      App.auth.authenticate(get_val('email'), get_val('password'));
    });

    document.querySelector('[auth-logout]').addEventListener('click', e => {
      App.pubsub.publish('auth:logout');
    });

    // Check for existing session
    this.authenticate();
  }

  authenticate(email, password) {
    let token = this.store.token || false;
    if (token) {
      document.body.setAttribute('authenticated', '');
      App.pubsub.publish('auth:login', { token: this.store.token });
    } else if (email && password) {
      this.request_token(email, password);
    }
  }

  request_token(email, password) {
    let self = this;
    App.pubsub.publish('auth:loading');
    qwest.post(App.url.token, {
      email: email,
      password: password
    })
      .then((xhr, response) => {
        if (response.success) {
          self.token = response.data.token;
          self.email = email;
          App.pubsub.publish('auth:login', { token: this.store.token });
        } else {
          App.pubsub.publish('auth:error', response.error);  
        }
      })
      .catch((error, xhr, response) => {
        throw new Error(error);
      })
      .complete(() => {
        // console.log('token request completed..');
      });
  }

  get token() {
    let token = this.store.token || false;

    if (token) {
      return this.store.token;
    } else {
      throw new Error('No token exists. Authenticate to get a token.');
    }
  }

  set token(token) {
    this.store.token = token;
  }

  get email() {
    let email = this.store.email || false;

    if (email) {
      return this.store.email;
    } else {
      throw new Error('No email exists. Authenticate to set an email.');
    }
  }

  set email(email) {
    this.store.email = email;
  }

  get logout() {
    App.pubsub.publish('auth:loading');
    this.store.removeItem('token');
    this.store.removeItem('email');
    delete App.user;
  }

  get profile_url() {
    return App.url.profile + this.email;
  }

  get headers() {
    return {
      headers: {
        'authorization': `Bearer ${this.token}`
      }
    };
  }

  get_profile(email) {
    let opts = {
      'filters[email][eq]': email  
    };
    
    qwest.get(App.url.profile, opts, this.headers)
      .then((xhr, response) => {
        App.user = response.data[0];
      })
      .catch((error, xhr, response) => {
        throw new Error(error);
      })
      .complete(() => {
        // console.log('profile request complete..');
      });
  }
}

export default AuthAjax;
