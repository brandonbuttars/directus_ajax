export let url = {
  base: 'http://localhost',
  api: '/api/1.1',
  token: '/auth/request-token',
  profile: '/tables/directus_users/rows'
};

export let qwest = {
  responseType: 'json',
  headers: {
    'authorization': `Bearer ${self.token}`
  }
};
