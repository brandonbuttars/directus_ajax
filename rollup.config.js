import nodeResolve from 'rollup-plugin-node-resolve';

export default {
  input: 'src/index.js',
  output: {
    file: 'dist/directus.ajax.js',
    format: 'es'
  },
  plugins: [
    nodeResolve({
      jsnext: true,
      main: true
    })
  ],
  globals: {
    qwest: 'qwest'
  }
};
